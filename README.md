# problemata-cci-index

Index des expositions du Centre de Création Industrielle ([link](http://problemata.org/visualisations/cci/)) pour [problemata.org](http://problemata.org/).

![Les Immatériaux](snapshots/les-immateriaux.png "Les Immatériaux")

## License

GNU Affero General Public License v3.0. 
