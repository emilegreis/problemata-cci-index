<?php namespace ProcessWire; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
        <meta name="HandheldFriendly" content="True" />
		<title><?php echo $page->title; ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>assets/reset.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>styles/index.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>fonts/freefont-20120503/fontfaces.css" />
		<script defer src="<?php echo $config->urls->templates; ?>scripts/index.js"></script>
	</head>
	<body data-header="open" data-type="intro">
        <section id="detail">
            <div id="header">
                <div id="title"><span id="span"><?php echo $page->title; ?></span></div>
                <div id="info"></div>
                <div onclick="header()" id="more"></div>
            </div>
            <div id="text">
                <?php echo $page->texte; ?>
            </div>
        </section>
        <section id="timeline">
            <div id="expositions"></div>
            <div id="annees"></div>
        </section>
        <section id="index"></section>
        <section id="colophon">
            <a href="http://problemata.org/fr/" target="_blank">problemata.org</a>
             – 
            <a href="https://gitlab.com/emilegreis/problemata-cci-index" target="_blank">code source</a>
        </section>
	</body>
</html>
