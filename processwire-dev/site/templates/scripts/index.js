// UTILS

function sort_arr_int_asc(arr, key) {
    arr.sort((a, b) => {
        return a[key] - b[key];
    });
    return arr
}

function sort_arr_str_asc(arr, key) {
    arr.sort((a, b) => {
        if (a[key].toLowerCase() < b[key].toLowerCase()) { return -1; }
        if (a[key].toLowerCase() >  b[key].toLowerCase()) { return 1; }
        return 0;
    });
    return arr
}

function int_to_month(n) {
    var months = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']
    return months[n-1]
}



// DETAIL

function create_span(obj) {
    var elt = document.createElement('span')
    elt.dataset.title = obj.title
    elt.dataset.type = obj.type
    elt.innerText = obj.title
    elt.addEventListener('click', click.bind(obj), false)
    return elt
}

function create_detail(obj) {
    document.getElementById('span').innerText = obj.title
    document.getElementById('info').innerHTML = types[obj.type].singular + '<br/>' + obj.counter + ' relation'
    if (obj.counter > 1) document.getElementById('info').innerHTML += 's'
    document.body.dataset.title = obj.title
    document.body.dataset.type = obj.type
    var nodes = text[obj.type](obj)
    while (document.getElementById('text').lastChild) document.getElementById('text').removeChild(document.getElementById('text').lastChild)
    for (var i = 0; i < nodes.length; i++) {
        document.getElementById('text').appendChild(nodes[i])
    }
}

text = {}

function crossmatches(obj) {
    var nodes = []
    nodes.push(document.createElement("br"))
    if (obj.links.matches.length > 0) {
        //nodes.push(document.createTextNode("Voir aussi en tant que "))
        nodes.push(document.createTextNode("Voir aussi : "))
        for (var i = 0; i < obj.links.matches.length; i++) {
            var match = create_span(obj.links.matches[i])
            //match.innerText = types[obj.links.matches[i].type].singular
            nodes.push(match)
            nodes.push(document.createTextNode(" (" + types[obj.links.matches[i].type].singular + ")"))
            i == obj.links.matches.length-2 ? nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode(". "))
    }
    return nodes
}

text.annee = function(obj) {
    var nodes = []
    return nodes
}

text.pays = function(obj) {
    var nodes = []
    return nodes
}

text.commune = function(obj) {
    var nodes = []
    return nodes
}

text.lieu = function(obj) {
    var nodes = []
    if (obj.links.exposition.length > 0) {
        //nodes.push(create_span(obj))
        nodes.push(document.createTextNode("Accueille "))
        obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" les expositions ")) : nodes.push(document.createTextNode(" l'exposition "))
        for (var i = 0; i < obj.links.exposition.length; i ++) {
            nodes.push(create_span(obj.links.exposition[i]))
            i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    return nodes
}

text.espace = function(obj) {
    var nodes = []
    if (obj.links.exposition.length > 0) {
        //nodes.push(create_span(obj))
        nodes.push(document.createTextNode("Accueille "))
        obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" les expositions ")) : nodes.push(document.createTextNode(" l'exposition "))
        for (var i = 0; i < obj.links.exposition.length; i ++) {
            nodes.push(create_span(obj.links.exposition[i]))
            i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    return nodes
}

text.commissaire = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Commissaire "))
    obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" des expositions ")) : nodes.push(document.createTextNode(" de l'exposition "))
    for (var i = 0; i < obj.links.exposition.length; i ++) {
        nodes.push(create_span(obj.links.exposition[i]))
        i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.scenographe = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Scénographe "))
    obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" des expositions ")) : nodes.push(document.createTextNode(" de l'exposition "))
    for (var i = 0; i < obj.links.exposition.length; i ++) {
        nodes.push(create_span(obj.links.exposition[i]))
        i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.theoricien = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Théoricien lié "))
    obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" aux expositions ")) : nodes.push(document.createTextNode(" à l'exposition "))
    for (var i = 0; i < obj.links.exposition.length; i ++) {
        nodes.push(create_span(obj.links.exposition[i]))
        i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.designer = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Designer lié "))
    obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" aux expositions ")) : nodes.push(document.createTextNode(" à l'exposition "))
    for (var i = 0; i < obj.links.exposition.length; i ++) {
        nodes.push(create_span(obj.links.exposition[i]))
        i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.auteur = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    //nodes.push(document.createTextNode(" a contribué aux textes "))
    nodes.push(document.createTextNode("Auteur des textes"))
    obj.links.catalogue.length > 1 ?  nodes.push(document.createTextNode(" des catalogues ")) : nodes.push(document.createTextNode(" du catalogue "))
    for (var i = 0; i < obj.links.catalogue.length; i ++) {
        nodes.push(create_span(obj.links.catalogue[i]))
        i == obj.links.catalogue.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.designer_graphique = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Designer graphique "))
    obj.links.catalogue.length > 1 ?  nodes.push(document.createTextNode(" des catalogues ")) : nodes.push(document.createTextNode(" du catalogue "))
    for (var i = 0; i < obj.links.catalogue.length; i ++) {
        nodes.push(create_span(obj.links.catalogue[i]))
        i == obj.links.catalogue.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.directeur = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Directeur "))
    obj.links.catalogue.length > 1 ?  nodes.push(document.createTextNode(" des catalogues ")) : nodes.push(document.createTextNode(" du catalogue "))
    for (var i = 0; i < obj.links.catalogue.length; i ++) {
        nodes.push(create_span(obj.links.catalogue[i]))
        i == obj.links.catalogue.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.directeur_du_centre_de_creation_industrielle = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Directeur du centre de création industrielle durant ")) // lié ?
    obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" les expositions ")) : nodes.push(document.createTextNode(" l'exposition "))
    for (var i = 0; i < obj.links.exposition.length; i ++) {
        nodes.push(create_span(obj.links.exposition[i]))
        i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.directeur_des_arts_decoratifs = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Directeur des Arts décoratifs durant ")) // lié ?
    obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" les expositions ")) : nodes.push(document.createTextNode(" l'exposition "))
    for (var i = 0; i < obj.links.exposition.length; i ++) {
        nodes.push(create_span(obj.links.exposition[i]))
        i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.president_du_centre_georges_pompidou = function(obj) {
    var nodes = []
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Président du centre Georges-Pompidou durant ")) // lié ?
    obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" les expositions ")) : nodes.push(document.createTextNode(" l'exposition "))
    for (var i = 0; i < obj.links.exposition.length; i ++) {
        nodes.push(create_span(obj.links.exposition[i]))
        i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    nodes = nodes.concat(crossmatches(obj))
    return nodes
}

text.exposition = function(obj) {
    var nodes = []
    var plurals = [
        'Halles de Baltard'
    ]
    if ((obj.date_debut && obj.date_fin) || (obj.sites.length > 0)) {
        nodes.push(document.createTextNode("L'exposition "))
        nodes.push(create_span(obj))
        nodes.push(document.createTextNode(" se déroule "))
        if (obj.date_debut && obj.date_fin) {
            nodes.push(document.createTextNode(" du "))
            nodes.push(document.createTextNode(obj.date_debut.split('-')[2]))
            nodes.push(document.createTextNode(" "))
            nodes.push(document.createTextNode(int_to_month(obj.date_debut.split('-')[1])))
            nodes.push(document.createTextNode(" "))
            nodes.push(create_span(types.annee.links.find(element => element.title === obj.date_debut.split('-')[0])))
            nodes.push(document.createTextNode(" au "))
            nodes.push(document.createTextNode(obj.date_fin.split('-')[2]))
            nodes.push(document.createTextNode(" "))
            nodes.push(document.createTextNode(int_to_month(obj.date_fin.split('-')[1])))
            nodes.push(document.createTextNode(" "))
            nodes.push(create_span(types.annee.links.find(element => element.title === obj.date_fin.split('-')[0])))
        }
        if (obj.sites) {
            for (var i = 0; i < obj.sites.length; i ++) {
                var path = obj.sites[i].split('|')
                plurals.includes(path[2].trim()) ? nodes.push(document.createTextNode(" aux ")) : nodes.push(document.createTextNode(" au "))
                nodes.push(create_span(types.lieu.links.find(element => element.title === path[2])))
                if (path.length == 4) {
                    nodes.push(document.createTextNode(" – "))
                    nodes.push(create_span(types.espace.links.find(element => element.title === path[3])))
                }
            }
        }
        nodes.push(document.createTextNode("."))
    }
    if (obj.links.producteur.length > 0) {
        nodes.push(document.createTextNode(" L'exposition est produite par "))
        for (var i = 0; i < obj.links.producteur.length; i ++) {
            nodes.push(create_span(obj.links.producteur[i]))
            i == obj.links.producteur.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    if (obj.links.commissaire.length > 0) {
        nodes.push(document.createTextNode(" Le commissariat est assuré par "))
        for (var i = 0; i < obj.links.commissaire.length; i ++) {
            nodes.push(create_span(obj.links.commissaire[i]))
            i == obj.links.commissaire.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    if (obj.links.scenographe.length > 0) {
        nodes.push(document.createTextNode(" La scénographie est réalisée par "))
        for (var i = 0; i < obj.links.scenographe.length; i ++) {
            nodes.push(create_span(obj.links.scenographe[i]))
            i == obj.links.scenographe.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    if (obj.links.catalogue.length> 0) {
        obj.links.catalogue.length > 1 ? nodes.push(document.createTextNode(" Les catalogues ")) : nodes.push(document.createTextNode(" Le catalogue "))
        for (var i = 0; i < obj.links.catalogue.length; i ++) {
            nodes.push(create_span(obj.links.catalogue[i]))
            i == obj.links.catalogue.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        obj.links.catalogue.length > 1 ? nodes.push(document.createTextNode(" sont ")) : nodes.push(document.createTextNode(" est "))
        nodes.push(document.createTextNode("produit a l'occasion."))
    }
    if (obj.itinerances.length > 0) {
        nodes.push(document.createTextNode(" L'exposition est visible (itinérances) dans les lieux suivants : "))
        for (var i = 0; i < obj.itinerances.length; i ++) {
            var lieu = types.lieu.links.find(link => link.title == obj.itinerances[i].site.split("|")[2])
            var commune = types.commune.links.find(link => link.title == obj.itinerances[i].site.split("|")[1])
            nodes.push(create_span(lieu))
            nodes.push(document.createTextNode(" – "))
            nodes.push(create_span(commune))
            i == obj.itinerances.length-2 ? nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    if (obj.links.article.length > 0) {
        obj.links.article.length > 1 ? nodes.push(document.createTextNode(" Les articles ")) : nodes.push(document.createTextNode(" L'article "))
        for (var i = 0; i < obj.links.article.length; i ++) {
            var a = create_span(obj.links.article[i])
            nodes.push(a)
            i == obj.itinerances.length-2 ? nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        obj.links.article.length > 1 ? nodes.push(document.createTextNode(" portent ")) : nodes.push(document.createTextNode(" porte "))
        nodes.push(document.createTextNode(" sur cette exposition"))
        nodes.push(document.createTextNode("."))
    }
    return nodes 
}

text.producteur = function(obj) {
    var nodes = []
    if (obj.links.exposition.length > 0) {
        //nodes.push(create_span(obj))
        nodes.push(document.createTextNode("Producteur"))
        obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" des expositions ")) : nodes.push(document.createTextNode(" de l'exposition "))
        for (var i = 0; i < obj.links.exposition.length; i ++) {
            nodes.push(create_span(obj.links.exposition[i]))
            i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    return nodes
}

text.article = function(obj) {
    var nodes = []
    //nodes.push(document.createTextNode("L'article "))
    //nodes.push(create_span(obj))
    nodes.push(document.createTextNode("Article portant sur "))
    obj.links.exposition.length > 1 ?  nodes.push(document.createTextNode(" les expositions ")) : nodes.push(document.createTextNode(" l'exposition "))
    for (var i = 0; i < obj.links.exposition.length; i ++) {
        nodes.push(create_span(obj.links.exposition[i]))
        i == obj.links.exposition.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
    }
    nodes.pop()
    nodes.push(document.createTextNode(". "))
    if (obj.adresse) {
        nodes.push(document.createElement('br'))
        var a = document.createElement('a')
        a.href = obj.adresse
        a.target = "_blank"
        a.innerText = "Lire l'article →"
        nodes.push(a)
    }
    return nodes
}

text.catalogue = function(obj) {
    var nodes = []
    if (obj.links.exposition.length > 0) {
        nodes.push(document.createTextNode("Le catalogue "))
        nodes.push(create_span(obj))
        nodes.push(document.createTextNode(" est publié"))
        if (obj.links.annee[0]) {
            nodes.push(document.createTextNode(" en "))
            nodes.push(create_span(obj.links.annee[0]))
        }
        if (obj.links.editeur.length > 0) {
            nodes.push(document.createTextNode(" par "))
            for (var i = 0; i < obj.links.editeur.length; i ++) {
                nodes.push(create_span(obj.links.editeur[i]))
                i == obj.links.editeur.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
            }
            nodes.pop()
        }
        if (obj.links.exposition.length > 0) {
            nodes.push(document.createTextNode(" dans le cadre de l'exposition "))
            nodes.push(create_span(obj.links.exposition[0]))
        }
        nodes.push(document.createTextNode("."))
    }
    if (obj.links.directeur.length > 0) {
        nodes.push(document.createTextNode(" L'ouvrage est dirigé par "))
        for (var i = 0; i < obj.links.directeur.length; i ++) {
            nodes.push(create_span(obj.links.directeur[i]))
            i == obj.links.directeur.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    if (obj.links.auteur.length > 0) {
        nodes.push(document.createTextNode(" Les textes sont écrits par "))
        for (var i = 0; i < obj.links.auteur.length; i ++) {
            nodes.push(create_span(obj.links.auteur[i]))
            i == obj.links.auteur.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    if (obj.links.designer_graphique.length > 0) {
        nodes.push(document.createTextNode(" Le design du catalogue est réalisé par "))
        for (var i = 0; i < obj.links.designer_graphique.length; i ++) {
            nodes.push(create_span(obj.links.designer_graphique[i]))
            i == obj.links.designer_graphique.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    return nodes
}

text.editeur = function(obj) {
    var nodes = []
    if (obj.links.catalogue.length > 0) {
        //nodes.push(create_span(obj))
        nodes.push(document.createTextNode("Éditeur"))
        obj.links.catalogue.length > 1 ?  nodes.push(document.createTextNode(" des catalogues ")) : nodes.push(document.createTextNode(" du catalogue "))
        for (var i = 0; i < obj.links.catalogue.length; i ++) {
            nodes.push(create_span(obj.links.catalogue[i]))
            i == obj.links.catalogue.length-2 ?  nodes.push(document.createTextNode(" et ")) : nodes.push(document.createTextNode(", "))
        }
        nodes.pop()
        nodes.push(document.createTextNode("."))
    }
    return nodes
}




// DOM

function create_separator(obj) {
    var elt = document.createElement('h2')
    elt.dataset.title = obj.title
    elt.dataset.counter = obj.links.length
    elt.innerText = obj.title
    obj.elt = elt 
    return elt
}



function create_button(obj) {
    obj.counter = 0
    for (var i in obj.links) obj.counter += obj.links[i].length
    var elt = document.createElement('button')
    elt.dataset.title = obj.title
    elt.dataset.type = obj.type
    elt.dataset.state = obj.state
    elt.dataset.counter = obj.counter
    elt.innerText = obj.title
    elt.addEventListener('click', click.bind(obj), false)
    elt.addEventListener('mouseenter', enter.bind(obj), false)
    elt.addEventListener('mouseleave', leave.bind(obj), false)
    obj.elt = elt 
    return elt 
}


function create_block(date_debut, date_fin, site, firstDate, totalDays) {
    if (date_debut && date_fin) {
        if (date_debut < date_fin) {
            var dateStart = new Date(
                date_debut.split("-")[0],
                date_debut.split("-")[1]-1,
                date_debut.split("-")[2],
                00, 00, 00
            )
            var dateEnd = new Date(
                date_fin.split("-")[0],
                date_fin.split("-")[1]-1,
                date_fin.split("-")[2],
                00, 00, 00
            )
            var durationDays = Math.round((dateEnd-dateStart)/(1000*60*60*24))
            var beforeDays = Math.round((dateStart-firstDate)/(1000*60*60*24))
            var div = document.createElement('div')
            div.style.left = beforeDays * 100 / totalDays + '%'
            div.style.width = durationDays * 100 / totalDays + '%'
            div.title = date_debut.split("-")[2] + '/' + date_debut.split("-")[1] + '/' + date_debut.split("-")[0] 
                + ' au ' + date_fin.split("-")[2] + '/' + date_fin.split("-")[1] + '/' + date_fin.split("-")[0] 
                + ' – '  + site.split('|')[2] + ' (' + site.split('|')[1] + ')'
            return div
        }
    }
}


function types_to_dom(types) {

    // index
    for (var i in types) {
        if (types[i].links.length > 0) {
            var item = document.createElement('div')
            item.classList.add('item')
            item.dataset.type = i
            document.getElementById('index').appendChild(item)
            var div = document.createElement('div')
            div.appendChild(create_separator(types[i]))
            for (var j = 0; j < types[i].links.length; j ++) {
                div.appendChild(create_button(types[i].links[j]))
            }
            item.appendChild(div)
        }
    }


    // timeline
    sort_arr_str_asc(types.exposition.links, 'date_debut')
    var firstYear = Number(types.annee.links[0].title)
    var lastYear = Number(types.annee.links[types.annee.links.length-1].title)
    for (var i = firstYear; i <= lastYear; i++) {
        var div = document.createElement('div')
        if (types.annee.links.some(link => link.title == i)) {
            var annee = types.annee.links.find(link => link.title == i)
            var button = document.createElement('button')
            button.innerText = i
            button.addEventListener('click', click.bind(annee), false)
            button.addEventListener('mouseenter', enter.bind(annee), false)
            button.addEventListener('mouseleave', leave.bind(annee), false)
            annee.elt2 = button;
            div.appendChild(button)
        } else {
            div.innerText = i
        }
        document.getElementById('annees').appendChild(div)
    }
    var firstDate = new Date(firstYear+'-01-01')
    var lastDate = new Date(lastYear+1+'-01-01')
    var totalDays = Math.round((lastDate-firstDate)/(1000*60*60*24))
    for (var i = 0; i < types.exposition.links.length; i++) {
        var exposition = types.exposition.links[i];
        var row = document.createElement('div')
        var left = document.createElement('div')
        var button = document.createElement('button')
        button.innerText = exposition.title
        left.appendChild(button)
        row.appendChild(left)
        var right = document.createElement('div')
        row.appendChild(right)
        row.addEventListener('click', click.bind(exposition), false)
        row.addEventListener('mouseenter', enter.bind(exposition), false)
        row.addEventListener('mouseleave', leave.bind(exposition), false)
        document.getElementById('expositions').appendChild(row)
        exposition.elt2 = row
        var display = false
        for (var j = 0; j < exposition.itinerances.length; j ++) {
            var div = create_block(exposition.itinerances[j].date_debut, exposition.itinerances[j].date_fin, exposition.itinerances[j].site, firstDate, totalDays)
            if (div) {
                right.appendChild(div)
                display = true
            }

        }
        if (exposition.date_debut && exposition.date_fin) {
            var div = create_block(exposition.date_debut, exposition.date_fin, exposition.sites[0], firstDate, totalDays)
            if (div) {
                right.appendChild(div)
                display = true
            }
        }
        if (!display) {
            exposition.elt2.style.display="none";
        }
        
    }


}



function update_dom() {
    for (var i in types) {
        for (var j in types[i].links) {
            types[i].links[j].elt.dataset.state = types[i].links[j].state
            types[i].links[j].elt.dataset.hover = types[i].links[j].hover
            if (i == 'exposition' || i == 'annee') {
                types[i].links[j].elt2.dataset.state = types[i].links[j].state
                types[i].links[j].elt2.dataset.hover = types[i].links[j].hover
            }
        }
    }
}



function click() {
    console.log(this)
    var deeper = this.state == 1 ? true : false
    for (var i in types) {
        for (var j in types[i].links) {
            types[i].links[j].state = 0
            types[i].links[j].hover = 0
        }
    }

    this.state = 1;
    for (var i in this.links) {
        for (var j = 0; j < this.links[i].length; j ++) {
            this.links[i][j].state = 2;
            if (deeper && this.type !== 'exposition') {
                for (var k in this.links[i][j].links) {
                    for (var l = 0; l < this.links[i][j].links[k].length; l ++) {
                        if (this.links[i][j].links[k][l].state !== 2 && this.links[i][j].links[k][l].state !== 1) this.links[i][j].links[k][l].state = 3
                    }
                }
            }
        }
    }

    create_detail(this)
    update_dom();
}



function enter() {
    this.hover = 1 
    for (var i in this.links) {
        for (var j = 0; j < this.links[i].length; j ++) {
            this.links[i][j].hover = 2;
        }
    }
    update_dom();
}



function leave() {
    this.hover = 0 
    for (var i in this.links) {
        for (var j = 0; j < this.links[i].length; j ++) {
            this.links[i][j].hover = 0;
        }
    }
    update_dom();
}



// OBJ

var types = {}

create_type('annee', 'Années', 'Année')
create_type('exposition', 'Expositions', 'Exposition')
create_type('pays', 'Pays', 'Pays')
create_type('commune', 'Communes', 'Commune')
create_type('lieu', 'Lieux', 'Lieu')
create_type('espace', 'Espaces', 'Espace')
create_type('producteur', 'Producteurs', 'Producteur')
create_type('commissaire', 'Commissaires', 'Commissaire')
create_type('scenographe', 'Scénographes', 'Scénographe')
create_type('theoricien', 'Théoriciens', 'Théoricien')
create_type('designer', 'Designers', 'Designer')
create_type('directeur_du_centre_de_creation_industrielle', 'Directeurs du CCI', 'Directeur du CCI')
create_type('directeur_des_arts_decoratifs', 'Dir. Arts décoratifs', 'Dir. Arts décoratifs')
create_type('president_du_centre_georges_pompidou', 'Prés. Centre Georges-Pompidou', 'Prés. Centre Georges-Pompidou')
create_type('catalogue', 'Catalogues', 'Catalogue')
create_type('editeur', 'Éditeurs', 'Éditeur')
create_type('directeur', 'Directeurs de catalogue', 'Directeur de catalogue')
create_type('auteur', 'Auteurs', 'Auteur')
create_type('designer_graphique', 'Designers graphiques', 'Designer Graphique')
create_type('article', 'Articles', 'Article')



function create_type(type, title, singular) {
    types[type] = {};
    types[type].links = [];
    types[type].title = title;
    types[type].singular = singular;
}



function link_obj(obj1, type, obj2) {
    if (!obj1.links[type].includes(obj2)) obj1.links[type].push(obj2)
}



function appendArray(refObj, type, array) {
    for (var i = 0; i < array.length; i++) {
        var obj = create_obj(array[i], type) 
        link_obj(obj, refObj.type, refObj)
        link_obj(refObj, type, obj)
    }
}



function appendValue(refObj, type, value) {
    if (value) {
        var obj = create_obj(value, type) 
        link_obj(obj, refObj.type, refObj)
        link_obj(refObj, type, obj)
    }
}



function appendSite(refObj, site) {
    if (site) {
        var path = site.split('|')
        if (path[0]) {
            var pays = create_obj(path[0], 'pays') 
            link_obj(pays, refObj.type, refObj)
            link_obj(refObj, 'pays', pays)
        }
        if (path[1]) {
            var commune = create_obj(path[1], 'commune') 
            link_obj(commune, refObj.type, refObj)
            link_obj(refObj, 'commune', commune)
            link_obj(commune, 'pays', pays)
            link_obj(pays, 'commune', commune)
        }
        if (path[2]) {
            var lieu = create_obj(path[2], 'lieu') 
            link_obj(lieu, refObj.type, refObj)
            link_obj(refObj, 'lieu', lieu)
            link_obj(lieu, 'pays', pays)
            link_obj(pays, 'lieu', lieu)
            link_obj(lieu, 'commune', commune)
            link_obj(commune, 'lieu', lieu)
        }
        if (path[3]) {
            var espace = create_obj(path[3], 'espace') 
            link_obj(espace, refObj.type, refObj)
            link_obj(refObj, 'espace', espace)
            link_obj(espace, 'pays', pays)
            link_obj(pays, 'espace', espace)
            link_obj(espace, 'commune', commune)
            link_obj(commune, 'espace', espace)
            link_obj(espace, 'lieu', lieu)
            link_obj(lieu, 'espace', espace)
        }
    }
}



function create_obj(title, type) {
    if (types[type].links.some(link => link.title == title)) return types[type].links.find(link => link.title == title)
    var obj = {}
    obj.state = 0
    obj.hover = 0
    obj.title = title
    obj.type = type
    obj.links = {}
    obj.links.annee = []
    obj.links.auteur = []
    obj.links.article = []
    obj.links.catalogue = []
    obj.links.commissaire = []
    obj.links.directeur_du_centre_de_creation_industrielle = []
    obj.links.directeur_des_arts_decoratifs = []
    obj.links.president_du_centre_georges_pompidou = []
    obj.links.designer = []
    obj.links.designer_graphique = []
    obj.links.directeur = []
    obj.links.editeur = []
    obj.links.exposition = []
    obj.links.itinerance = []
    obj.links.producteur = []
    obj.links.scenographe = []
    obj.links.theoricien = []
    obj.links.pays = []
    obj.links.commune = []
    obj.links.lieu = []
    obj.links.espace = []
    obj.links.matches = []
    obj.links.timeline = []
    if (!types[type].links.includes(obj)) types[type].links.push(obj)
    return obj
}



function json_to_types(json) {
    
    // catalogues
    for (var i = 0; i < json.catalogues.length; i++) {
        var catalogue = create_obj(json.catalogues[i].title, 'catalogue')
        //catalogue.annee = json.catalogues[i].toString;
        appendValue(catalogue, 'annee', json.catalogues[i].annee.toString())
        appendArray(catalogue, 'directeur', json.catalogues[i].directeurs)
        appendArray(catalogue, 'auteur', json.catalogues[i].auteurs)
        appendArray(catalogue, 'designer_graphique', json.catalogues[i].designers_graphiques)
        appendArray(catalogue, 'editeur', json.catalogues[i].editeurs)
        appendSite(catalogue, json.catalogues[i].commune)
    }
   
    // expositions
    for (var i = 0; i < json.expositions.length; i++) {
        var exposition = create_obj(json.expositions[i].title, 'exposition')
        exposition.date_debut = json.expositions[i].date_debut
        exposition.date_fin = json.expositions[i].date_fin
        exposition.itinerances = json.expositions[i].itinerances
        exposition.sites = []
        if (exposition.date_debut && exposition.date_fin) { 
            for (var j = exposition.date_debut.split('-')[0]; j <= exposition.date_fin.split('-')[0]; j ++) {
                appendValue(exposition, 'annee', j.toString())
            }
        } else if (exposition.date_debut) { 
            appendValue(exposition, 'annee', exposition.date_debut.split('-')[0].toString())
        } else if (exposition.date_fin) { 
            appendValue(exposition, 'annee', exposition.date_fin.split('-')[0].toString())
        }
        appendArray(exposition, 'producteur', json.expositions[i].producteurs)
        appendArray(exposition, 'commissaire', json.expositions[i].commissaires)
        appendArray(exposition, 'scenographe', json.expositions[i].scenographes)
        appendArray(exposition, 'theoricien', json.expositions[i].theoriciens)
        appendArray(exposition, 'designer', json.expositions[i].designers)
        appendArray(exposition, 'directeur_du_centre_de_creation_industrielle', json.expositions[i].directeurs_du_centre_de_creation_industrielle)
        appendArray(exposition, 'directeur_des_arts_decoratifs', json.expositions[i].directeurs_des_arts_decoratifs)
        appendArray(exposition, 'president_du_centre_georges_pompidou', json.expositions[i].presidents_du_centre_georges_pompidou)
        appendArray(exposition, 'catalogue', json.expositions[i].catalogues)
        for (var j = 0; j < json.expositions[i].catalogues.length; j ++) {
            var catalogue = create_obj(json.expositions[i].catalogues[j], 'catalogue');
            for (var k = 0; k < catalogue.links.directeur.length; k ++) {
                link_obj(catalogue.links.directeur[k], 'exposition', exposition)
                link_obj(exposition, 'directeur', catalogue.links.directeur[k])
            }
        }
        for (var j = 0; j < json.expositions[i].articles.length; j ++) {
            var obj = create_obj(json.expositions[i].articles[j].title, 'article')
            link_obj(obj, 'exposition', exposition)
            link_obj(exposition, 'article', obj)
            obj.adresse = json.expositions[i].articles[j].adresse;
        }
        for (var j = 0; j < json.expositions[i].sites.length; j ++) {
            appendSite(exposition, json.expositions[i].sites[j])
            exposition.sites.push(json.expositions[i].sites[j])
        }
        for (var j = 0; j < json.expositions[i].itinerances.length; j ++) {
            var itinerance = json.expositions[i].itinerances[j]
            appendSite(exposition, itinerance.site)
            if (itinerance.date_debut && itinerance.date_fin) { 
                for (var k = itinerance.date_debut.split('-')[0]; k <= itinerance.date_fin.split('-')[0]; k ++) {
                    appendValue(exposition, 'annee', k.toString())
                }
            } else if (itinerance.date_debut) { 
                appendValue(exposition, 'annee', itinerance.date_debut.split('-')[0].toString())
            } else if (itinerance.date_fin) { 
                appendValue(exposition, 'annee', itinerance.date_fin.split('-')[0].toString())
            }
        }
    }

    // sort types
    for (var i in types) {
        types[i].links.sort((t1, t2) => t1.title.localeCompare(t2.title));
    }

    // cross links matches
    for (var i in types) {
        for (var j in types[i].links) {
            for (var k in types) {
                if (i !== k) {
                    var matches = types[k].links.filter(link => link.title === types[i].links[j].title)
                    for (var l = 0; l < matches.length; l ++) {
                        link_obj(types[i].links[j], 'matches', matches[l])
                        link_obj(matches[l], 'matches', types[i].links[j])
                    }
                }
            }
        }
    }
}



// INIT

fetch('data/') // distant
//fetch('http://localhost/~emile/emile/emilegreis.net/2021/problemata-cci-index/processwire-dev/data/') // local
//fetch('http://192.168.1.167/~emile/emile/emilegreis.net/2021/problemata-cci-index/processwire-dev/data/') // local
    .then(response => response.json())
    .then(json => {
        console.log(json);
        json_to_types(json)
        console.log(types)
        types_to_dom(types)
        resizeAllGridItems()
    });



// MINI CSS GRID MASONRY

function header() {
    document.body.dataset.header == 'close' ? document.body.dataset.header = 'open' : document.body.dataset.header = 'close'
}

function resizeGridItem(item){
    var grid = document.getElementById('index')
    var rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'))
    var rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'))
    var rowSpan = Math.ceil((item.getElementsByTagName('div')[0].getBoundingClientRect().height+rowGap)/(rowHeight+rowGap))
    item.style.gridRowEnd = "span "+rowSpan;
}

function resizeAllGridItems(){
    var allItems = document.getElementById('index').getElementsByClassName('item')
    for(i = 0; i < allItems.length; i++){
        resizeGridItem(allItems[i]);
    }
}

window.addEventListener("resize", resizeAllGridItems);
