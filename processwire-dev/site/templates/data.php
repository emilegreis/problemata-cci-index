<?php namespace ProcessWire; ?>

<?php 

$data = [];
$data['expositions'] = [];
$data['catalogues'] = [];
#$data['sites'] = [];

foreach ($pages->get('/data/expositions/')->children("sort=title") as $p) {
    $obj = [];
    foreach ($p->fields as $f) {
        if ($f == 'itinerances') {
            $obj['itinerances'] = [];
            foreach ($p->itinerances as $v) {
                $sub = [];
                $sub['date_debut'] = $v->date_debut;
                $sub['date_fin'] = $v->date_fin;
                $sub['site'] = '';
                foreach ($v->site->parents('template=lieu|commune|pays') as $parent) {
                    $sub['site'].=$parent->title.'|';
                }
                $sub['site'].= $v->site->title;
                array_push($obj['itinerances'], $sub);
            }
        } else if ($f == 'articles') {
            $obj['articles'] = [];
            foreach ($p->articles as $v) {
                $sub = [];
                $sub['title'] = $v->title;
                $sub['adresse'] = $v->adresse;
                array_push($obj['articles'], $sub);
            }
        } else if ($f == 'sites') {
            $obj['sites'] = [];
            foreach ($p->sites as $v) {
                array_push($obj['sites'], '');
                foreach ($v->parents('template=lieu|commune|pays') as $parent) {
                    $obj['sites'][count($obj['sites'])-1].=$parent->title.'|';
                }
                $obj['sites'][count($obj['sites'])-1].= $v->title;

            }
        } else if (gettype($p->$f) == 'integer') {
            $obj[(string)$f] = $p->$f;
        } else if (gettype($p->$f) == 'string') {
            $obj[(string)$f] = $p->$f;
        } else if (gettype($p->$f) == 'object') {
            $obj[(string)$f] = [];
            foreach ($p->$f as $v) {
                array_push($obj[(string)$f], $v->title);
            }
        }
    }
    array_push($data['expositions'], $obj);
}

foreach ($pages->get('/data/catalogues/')->children("sort=title") as $p) {
    $obj = [];
    foreach ($p->fields as $f) {
        if ($f == 'commune') {
            if ($p->$f) {
                $obj['commune'] = '';
                    foreach ($p->$f->parents('template=lieu|commune|pays') as $parent) {
                        $obj['commune'].=$parent->title.'|';
                    }
                $obj['commune'].= $p->$f->title;
            }
        } else if (gettype($p->$f) == 'integer') {
            $obj[(string)$f] = $p->$f;
        } else if (gettype($p->$f) == 'string') {
            $obj[(string)$f] = $p->$f;
        } else if (gettype($p->$f) == 'object') {
            $obj[(string)$f] = [];
            foreach ($p->$f as $v) {
                array_push($obj[(string)$f], $v->title);
            }
        }
    }
    array_push($data['catalogues'], $obj);
}

#foreach ($pages->get('/data/sites/')->children("sort=title") as $p) {
#    $pays = [];
#    $pays['title'] = $p->title;
#    $pays['communes'] = [];
#    foreach ($p->children("sort=title") as $c) {
#        $commune = [];
#        $commune['title'] = $c->title;
#        $commune['lieux'] = [];
#        foreach ($c->children("sort=title") as $l) {
#            $lieu = [];
#            $lieu['title'] = $l->title;
#            $lieu['espaces'] = [];
#            foreach ($l->children("sort=title") as $e) {
#                $espace = [];
#                $espace['title'] = $e->title;
#                array_push($lieu['espaces'], $pays);
#            }
#            array_push($commune['lieux'], $lieu);
#        }
#        array_push($pays['communes'], $commune);
#    }
#    array_push($data['sites'], $pays);
#}



header('Content-Type: application/json; charset=UTF-8');
#echo json_encode($data, JSON_PRETTY_PRINT);
echo json_encode($data, JSON_UNESCAPED_UNICODE);
exit();
